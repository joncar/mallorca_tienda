<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Usuario extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function ventas(){
            $crud = $this->crud_function('','');            
            $crud->where('user_id',$this->user->id);
            $crud->set_subject('Les teves compres');
            $crud->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_export()
                ->unset_print()
                ->display_as('Datos de entrega')                
                ->columns('procesado','fecha_compra','productos','dia_entrega','hora_entrega','provincias_id','forma_pago');
            
            $crud->display_as('fecha_compra','Data de compra')
                 ->display_as('procesado','Procesat')
                 ->display_as('productos','Productes');
            
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<a href="'.site_url('usuario/comprar/'.$row->id).'" class="label label-default">Per procesat</a>'; break;
                    case '2': return '<span class="label label-success">Procesat</span>'; break;
                }
            });
            
            $crud->callback_column('productos',function($val,$row){
                get_instance()->db->join('productos','productos.id = ventas_detalles.productos_id');
                $ventas = get_instance()->db->get_where('ventas_detalles',array('ventas_id'=>$row->id));
                $str = '';
                foreach($ventas->result() as $v){
                    $str.= $v->cantidad.' '.$v->producto_nombre.',';
                }
                return $str;
            });
            $output = $crud->render();
            $output->title = 'Les teves compres';
            $this->loadView($output);
        }    
        
        function comprar($id = ''){            
            if(!empty($_SESSION['carrito']) && empty($id)){
                $data = array('user_id'=>$this->user->id,'total'=>0,'fecha_compra'=>date("Y-m-d H:i:s"),'procesado'=>1);                
                $this->db->insert('ventas',$data);                
                $id = $this->db->insert_id();
                $total = 0;
                foreach($_SESSION['carrito'] as $n=>$v){
                    $this->db->insert('ventas_detalles',array('talla'=>$v->tallaselected,'color'=>$v->colorselected,'ventas_id'=>$id,'productos_id'=>$v->id,'cantidad'=>$v->cantidad,'monto'=>($v->precio*$v->cantidad)));
                    $total+= ($v->precio*$v->cantidad);                    
                }
                if($total>0){
                    $ajustes = $this->db->get('ajustes')->row();
                    if(strtotime(date("Y-m-d")) >= strtotime($ajustes->suspender_cobro_envio_desde) && strtotime(date("Y-m-d")) <= strtotime($ajustes->suspender_cobro_envio_hasta)){
                        $impuesto = 0;
                    }else{
                        $impuesto = 4.95;
                    }    
                    $impuesto = $total<25?$impuesto:0; 
                    $costo_envio = $impuesto;
                    $this->db->update('ventas',array('total'=>$total,'costo_envio'=>$costo_envio),array('id'=>$id));
                    unset($_SESSION['carrito']);
                    $this->loadView(array('view'=>'pagar','venta'=>$this->db->get_where('ventas',array('id'=>$id))->row(),'submit'=>true));                                
                }else{
                    $this->db->delete('ventas',$data);
                    throw new Exception('Disculpe pero esta acción no esta autorizada, será notificada',403);
                    die();
                }
            }else{
                if(empty($id) || !is_numeric($id)  || $id<0){
                    header("Location:".base_url('panel'));
                }else{
                    $venta = $this->db->get_where('ventas',array('id'=>$id));
                    if($venta->num_rows>0){                        
                        $this->loadView(array('view'=>'pagar','venta'=>$venta->row(),'redsys'=>$this->redsysapi,'submit'=>false));
                    }else{
                        header("Location:".base_url('panel'));
                    }
                }
            }
        }                
    }
?>
