<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Tablas extends Panel{
        function __construct() {
            parent::__construct();
               $this->load->library('image_crud');
        }
        
        function ubicaciones(){
            $crud = $this->crud_function('','');            
            $this->loadView($crud->render());
        }
        function colores(){
            $crud = $this->crud_function('','');            
            $this->loadView($crud->render());
        }
        function ejemplares(){
            $crud = $this->crud_function('','');            
            $this->loadView($crud->render());
        }
        
        function subscribe_list(){
            $this->as = array('subscribe_list'=>'subscribe');
            $crud = $this->crud_function('','');
            $crud->set_subject('Subscrito');
            $this->loadView($crud->render());
        }
        
        function banco_imagenes(){
            $crud = new image_CRUD();
            $crud->set_table('banco_imagenes')
                    ->set_url_field('foto')
                    ->set_image_path('images/banco')
                    ->set_ordering_field('priority')
                    ->module = 'admin';
            $this->loadView($crud->render());
        }
    }
?>
