<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    var $referidos = array();
    
    function __construct()
    {
        parent::__construct();
    } 
    
    function getBanner(){
        $this->db->order_by('priority','ASC');
        return $this->db->get_where('banner',array('visible'=>1));
    }
    
    function getCategorias(){
        return $this->db->get('categorias');
    }
    
    function getProductos(){
        if(!empty($_GET['categoria']) && is_numeric($_GET['categoria']) && $_GET['categoria']>0){
            $this->db->where('categorias_id',$_GET['categoria']);
        }
        if(!empty($_GET['producto'])){
            $this->db->like('producto_nombre',$_GET['producto']);
        }
        $this->db->order_by('priority','ASC');
        $this->db->where('disponible',1);
        return $this->db->get('productos');
    }
    
    function getCarrito(){
        if(empty($_SESSION['carrito'])){
            return array();
        }
        else{
            return $_SESSION['carrito'];
        }
    }
    
    function setCarrito($item,$sumarcantidad = TRUE){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }
        $existente = false;
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$item->id){
                if($sumarcantidad){
                    $_SESSION['carrito'][$n]->cantidad+=$item->cantidad;
                }else{
                    $_SESSION['carrito'][$n]->cantidad=$item->cantidad;
                }
                if(!empty($item->colorselected)){
                    $_SESSION['carrito'][$n]->colorselected=$item->colorselected;
                }
                if(!empty($item->tallaselected)){
                    $_SESSION['carrito'][$n]->tallaselected=$item->tallaselected;
                }
                $existente = true;
            }
        }
        if(!$existente){
            array_push($_SESSION['carrito'],$item);
        }        
    }
    
    function delCarrito($producto_id){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }        
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$producto_id){
                unset($_SESSION['carrito'][$n]);
            }
        }                
    }
    
    function getPage($titulo,$return = FALSE){
        $texto = $this->db->get_where('paginas',array('titulo'=>$titulo));
        $str = '';
        if($texto->num_rows>0){
            $str = $texto->row()->texto;
        }
        
        if($return){
            return $str;
        }
        else{
            echo $str;
        }
    }
    
    function getComentarios($productos_id){
        return $this->db->get_where('comentarios',array('productos_id'=>$productos_id));
    }
    
    function pagoOk($post){
        if(!empty($_SESSION['productocompra'])){                            
            $this->db->update('ventas',array('procesado'=>2),array('id'=>$_SESSION['productocompra']));                                    
            $this->db->select('
                ventas.id,
                user.nombre,
                user.apellido_paterno,
                user.email,
                user.ciudad,
                provincias.nombre_provincia,
                user.direccion,
                user.codigo_postal,
                user.telefono,                
                ventas.total,                
                ventas.costo_envio, 
                ventas.observaciones, 
                productos.producto_nombre, 
                ventas_detalles.cantidad, 
                ventas_detalles.monto, 
                ventas_detalles.talla,
                ventas_detalles.color
            ');            
            $this->db->join('user','user.id = ventas.user_id');
            $this->db->join('provincias','user.provincias_id = provincias.id');
            $this->db->join('ventas_detalles','ventas.id = ventas_detalles.ventas_id');
            $this->db->join('productos','productos.id = ventas_detalles.productos_id');
            $cliente = $this->db->get_where('ventas',array('ventas.id'=>$_SESSION['productocompra']));
            if($cliente->num_rows()>0){
                unset($_SESSION['productocompra']);
                $var = '';
                $this->db->select('producto_nombre as Producto, cantidad as Cantidad, monto as importe, ventas_detalles.talla as Talla, ventas_detalles.color as Color');
                $this->db->join('productos','productos.id = ventas_detalles.productos_id');
                foreach($this->db->get_where('ventas_detalles',array('ventas_id'=>$cliente->row()->id))->result() as $r){
                    foreach($r as $n=>$v){
                        $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';                    
                    }
                }
                
                $cliente->row()->productos = $var;
                return $cliente->row();
                
            }else{
                return false;
            }
        }else{
            return false;
        }
        
    }
    
    function pagoNoOk(){
        if(!empty($_SESSION['productocompra'])){                            
            $this->db->update('ventas',array('procesado'=>-1),array('id'=>$_SESSION['productocompra']));
            unset($_SESSION['productocompra']);
        }
    }
}
?>
