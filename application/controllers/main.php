<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();
class Main extends CI_Controller {

        public function __construct()
        {
                    parent::__construct();
                    $this->load->helper('url');
                    $this->load->helper('html');
                    $this->load->helper('h');
                    $this->load->database();
                    $this->load->model('user');                    
                    $this->load->library('grocery_crud');
                    $this->load->library('ajax_grocery_crud');
                    $this->load->model('querys'); 
                    $this->load->library('redsysAPI');
        }

        public function index()
        {            
            $data = array(
                'view'=>'main',
                'banner'=>$this->querys->getBanner(),
            );
            $this->loadView($data);                
        }

        public function success($msj)
        {
                return '<div class="alert alert-success">'.$msj.'</div>';
        }

        public function error($msj)
        {
                return '<div class="alert alert-danger">'.$msj.'</div>';
        }

        public function login()
        {
                if(!$this->user->log)
                {	
                        if(!empty($_POST['email']) && !empty($_POST['pass']))
                        {
                                $this->db->where('email',$this->input->post('email'));
                                $r = $this->db->get('user');
                                if($this->user->login($this->input->post('email',TRUE),$this->input->post('pass',TRUE)))
                                {
                                        if($r->num_rows>0 && $r->row()->status==1)
                                        {
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('panel').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
                                        }
                                        else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                                }
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                        }
                        else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url('registro/index/add'));
                }
                else header("Location:".base_url('registro/index/add'));
        }

        function pages($titulo){
            $titulo = urldecode(str_replace("-","+",$titulo));
            if(!empty($titulo)){
                $pagina = $this->db->get_where('paginas',array('titulo'=>$titulo));
                if($pagina->num_rows>0){
                    $this->loadView(array('view'=>'pages','texto'=>$pagina->row()->texto,'title'=>$titulo));                    
                }
                else $this->loadView('404');
            }
        }                

        public function unlog()
        {
                $this->user->unlog();                
                header("Location:".site_url());
        }

        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param)){
                $param = array('view'=>$param);
            }
            $this->load->view('template',$param);
        }

        public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
                
         function error404(){
            $this->loadView(array('view'=>'errors/403'));
        }
        
        function vistaprevia($id){
            $this->db->where('productos.id',$id);
            $producto = $this->querys->getProductos();
            if($producto->num_rows>0){
                $this->load->view('includes/fragmentos/vistaprevia',array('producto'=>$producto->row()));
            }
            else{
                echo 'Producto no encontrado';
            }
        }
        
        public function categoriasShow($id){
            $id = explode('-',$id);
            $id = $id[count($id)-1];
            if(is_numeric($id)){
                $_GET['categoria'] = $id;
                $this->index();
            }
            else{
                throw new Exception('No se encontro el producto seleccionado','404');
            }
        }
        
        public function productosShow($id,$mensaje = ''){
            $id = explode('-',$id);
            $id = $id[count($id)-1];
            if(is_numeric($id)){
                $this->db->select('productos.*, categorias.id as categoria_id, categorias.categoria_nombre');
                $this->db->join('categorias','categorias.id = productos.categorias_id');
                $this->db->where('productos.id',$id);
                $productos = $this->querys->getProductos();
                if($productos->num_rows>0){
                    if(!empty($_SESSION['msj'])){
                        $mensaje = 'El seu comentari ha estat registrat amb èxit';
                        unset($_SESSION['msj']);
                    }
                    $data = array(
                        'view'=>'productos',
                        'producto'=>$productos->row(),
                        'comentarios'=>$this->querys->getComentarios($productos->row()->id),
                        'mensaje'=>$mensaje
                    );
                    $this->loadView($data);               
                }
                else{
                    throw new Exception('No se encontro el producto seleccionado','404');
                }
            }
            else{
                throw new Exception('No se encontro el producto seleccionado','404');
            }
        }
        
        public function refreshCartForm(){
            $this->load->view('includes/fragmentos/carritoForm');
        }
        
        public function carrito($venta_id = ''){
            $this->loadView('carrito');
        }
        
        public function addComment(){
            if(!empty($_POST)){                
                $d = array();
                foreach($_POST as $n=>$p){
                    $this->form_validation->set_rules($n,$n,'required');
                    $d[$n] = $p;
                }

                if($this->form_validation->run()){
                    $d['fecha'] = date("Y-m-d H:i:s");
                    $this->db->insert('comentarios',$d);                    
                    echo 'success';
                    $_SESSION['msj'] = '1';
                }
            }
        }
        
        public function addToCart($prod = '',$cantidad = '',$color = '',$talla = '',$return = TRUE,$sumarcantidad = TRUE){
            if(!empty($prod) && is_numeric($prod) && $prod>0 && !empty($cantidad) && is_numeric($cantidad) && $cantidad>0){                
                $this->db->where('productos.id',$prod);
                $producto = $this->querys->getProductos();
                if($producto->num_rows>0){
                    $producto = $producto->row();
                    $producto->cantidad = $cantidad;
                    $producto->tallaselected = $talla;
                    $producto->colorselected = $color;
                    $this->querys->setCarrito($producto,$sumarcantidad);
                }                
            }
            if($return){
                $this->load->view('includes/fragmentos/carritonav');
            }
        }
        
        public function addToCartArray(){
            if(!empty($_POST)){                
                $response = 'success';
                foreach($_POST['id'] as $n=>$v){
                    if(empty($_POST['color'][$n]) && !empty($this->db->get_where('productos',array('id'=>$v))->row()->color)){
                        $response = "Sis plau tria un color";
                    }elseif(empty($_POST['talla'][$n]) && !empty($this->db->get_where('productos',array('id'=>$v))->row()->talla)){
                        $response = "Sis plau tria un talla";
                    }
                    else{
                        $this->addToCart($v,$_POST['cantidad'][$n],$_POST['color'][$n],$_POST['talla'][$n],FALSE,FALSE);
                    }
                }
                echo $response;                
            }else{
                echo "Sis plau completi totes les dades amb asterisc";
            }
        }
        
        public function delToCart($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->querys->delCarrito($prod);                
            }
            $this->load->view('includes/fragmentos/carritonav');
        }
        
        function contacto(){
            if(!empty($_POST)){
                foreach($_POST as $n=>$p){
                    $this->form_validation->set_rules($n,$n,'required');
                }
                
                if($this->form_validation->run()){
                    $str = '<p><b>Solicitud de Contacto</b></p>';
                    foreach($_POST as $n=>$p){
                        $str.= '<p><b>'.$n.': </b> '.$p.'</p>';
                    }
                    
                    correo($this->db->get('ajustes')->row()->correo,'Solicitud de Contacto',$str);
                    correo('joncar.c@gmail.com','Solicitud de Contacto',$str);
                    $mensaje = $this->success('El seu comentari ha estat registrat amb èxit');
                }
                else{
                    $mensaje = $this->error($this->form_validation->error_string());
                }
            }
            else{
                $mensaje = '';
            }
            $output = $this->db->get_where('paginas',array('titulo'=>'contactenos'))->row()->texto;     
            $output = str_replace('[ubicacion]',$this->load->view('includes/fragmentos/ubicacion',array(),TRUE),$output);
            $this->loadView(array('view'=>'pages','texto'=>$output));
        }
        
        function procesarPago(){
            /*$_SESSION['productocompra'] = 7;
            $cliente = $this->querys->pagoOk(array('test'=>'test'));
            
            if(!empty($cliente)){
                $this->enviarcorreo($cliente,3);
                $this->enviarcorreo($cliente,4);                
            }*/
            $this->load->library('redsysapi');
            $miObj = new RedsysAPI;
            if (!empty($_POST)){
                $datos = $_POST["Ds_MerchantParameters"];
                $decodec = json_decode($miObj->decodeMerchantParameters($datos));                
                if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){                    
                    $id = substr($decodec->Ds_Order,3);
                    $_SESSION['productocompra'] = $id;
                    $cliente = $this->querys->pagoOk($decodec);
                    if(!empty($cliente)){
                        $this->enviarcorreo($cliente,3);
                        $this->enviarcorreo($cliente,4,'tienda@mallorcaislandfestival.com');                
                    }
                }else{
                    $id = substr($decodec->Ds_Order,3);
                    $_SESSION['productocompra'] = $id;
                    $this->querys->pagoNoOk();
                }                                                
            }
        }
        
        public function pagoOk(){
            $this->loadView('pagook');            
        }
        
        public function pagoKo(){
            $this->loadView('pagoko');
        }
        
        protected function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{usuarios.'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{usuarios.'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            //correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
            //correo('info@hipo.tv',$mensaje->titulo,$mensaje->texto);
        }
        
        function mostrarlic(){
            echo $this->db->get_where('notificaciones',array('id'=>5))->row()->texto;
        }
}
