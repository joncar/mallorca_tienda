<?php $this->load->view('includes/template/header') ?>
<div id="main">    
    <div class="main-header background background-image-heading-product">
        <div class="container">
            <h1><?= $producto->producto_nombre ?></h1>
        </div>
    </div>

    <div id='beesocial' data-domain='<?= base_url() ?>' data-product-image='<?= base_url('images/productos/'.$producto->foto) ?>' data-url='<?= site_url('productos/'. toURL($producto->producto_nombre).'-'.$p->id) ?>' data-comment-width='525' data-css-style='padding-top: 20px; clear: both' platform='custom'>
    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<?php base_url() ?>">Inicio</a>
                </li>
                <li class="active"><span><?= $producto->producto_nombre ?></span>
                </li>
            </ol>

        </div>
    </div>


    <div class="container">
        <div class="product-slider-wrapper">
            <div class="swiper-container product-slider-thumbs hidden-xs">
                <div class="swiper-wrapper">
                    <?php foreach($this->db->get_where('fotos',array('productos_id'=>$producto->id))->result() as $f): ?>
                    <div class="swiper-slide">
                        <?= img('images/productos/'.$f->foto) ?>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
            <!-- /.swiper-container -->

            <div class="swiper-container product-slider-main">
                <div class="swiper-wrapper">
                    <?php foreach($this->db->get_where('fotos',array('productos_id'=>$producto->id))->result() as $f): ?>
                    <div class="swiper-slide">
                        <div class="easyzoom easyzoom--overlay">
                            <a href="<?= base_url('images/productos/'.$f->foto) ?>" title="">
                                <?= img('images/productos/'.$f->foto,'width:100%') ?>
                            </a>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>

                <div class="swiper-button-prev"><i class="fa fa-chevron-left"></i>
                </div>
                <div class="swiper-button-next"><i class="fa fa-chevron-right"></i>
                </div>
            </div>
            <!-- /.swiper-container -->

        </div>
        <!-- /.product-slider-wrapper -->
        <?php if(!empty($mensaje)): ?>
        <div class='alert alert-success'><?= $mensaje ?></div>
        <?php endif ?>

        <div class="row cols-border">
            <div class="col-md-6">
                <div class="product-details-wrapper">
                    <h2 class="product-name">
                        <a href="#" title="<?= $producto->producto_nombre ?>"> <?= $producto->producto_nombre ?></a>
                    </h2>
                    <!-- /.product-name -->                    

                    <div class="product-description">
                        <p><?= $producto->descripcion ?></p>
                    </div>
                    <!-- /.product-description -->                    

                    <div class="product-actions-wrapper">
                        <!-- /.form -->

                        <div class="product-list-actions">
                            <span class="product-price">
                            <span class="amount">
                                <del><?= $producto->precio_sin_descuento!=0?moneda($producto->precio_sin_descuento):'' ?></del>
                                <?= moneda($producto->precio) ?>
                            </span>
                            </span>
                            <!-- /.product-price -->

                            <a href="javascript:addToCart('<?= $producto->id ?>',1)" class="btn btn-lg btn-primary">Añadir al carrito</a>
                        </div>
                        <!-- /.product-list-actions -->
                    </div>
                    <!-- /.product-actions-wrapper -->

                    <div class="product-meta">
                        <span class="product-category">
                            <span>Categoria:</span>
                            <a href="<?= base_url('categorias/'.toURL($producto->categoria_nombre).'-'.$producto->id) ?>" title=""><?= $producto->categoria_nombre ?></a>
                        </span>                                               
                    </div>
                    <!-- /.product-meta -->
                </div>
                <!-- /.product-details-wrapper -->
            </div>

            <div class="col-md-6">
                <div role="tabpanel" class="product-details">
                    <nav>
                        <ul class="nav" role="tablist">
                         <li role="presentation">
                                <a href="#product-review" data-toggle="tab">Comentario <span>(<?= $comentarios->num_rows ?>)</span></a>
                            </li>
                            <li role="presentation" class="active">
                                <a href="#product-description" data-toggle="tab">Descripción</a>
                            </li>
                           
                        </ul>
                        <!-- /.nav -->
                    </nav>
                    <!-- /nav -->

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="product-description">
                            <?= $producto->descripcion_larga ?>
                        </div>                        

                        <div role="tabpanel" class="tab-pane" id="product-review">
                            <h3>Comentarios <span>(<?= $comentarios->num_rows ?>)</span></h3>

                            <ol class="product-review-list">
                                <?php foreach($comentarios->result() as $com): ?>
                                <li>
                                    <h4 class="review-title"><?= $com->titulo ?></h4>

                                    <div class="review-comment">
                                        <?= $com->comentario ?>
                                    </div>

                                    <div class="review-meta">
                                        <span>Escrit per</span>
                                        <a href="mailto:<?= $com->email ?>" class="author"><?= $com->autor ?></a>
                                        <span>-</span>
                                        <span><?= date("d-m-Y H:i:s",strtotime($com->fecha)) ?></span>
                                    </div>
                                </li>
                                <?php endforeach ?>
                            </ol>
                            <!-- /.product-review-list -->

                            <h3>Añadir Comentario</h3>

                            <form id='form' action="" method="POST" onsubmit='return sendComment()'>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="reply-name">Nombre <sup>*</sup>
                                            </label>
                                            <input type="text" name="autor" class="form-control" id="reply-name" placeholder="Nombre">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="reply-email">Email <sup>*</sup>
                                            </label>
                                            <input type="email" name="email" class="form-control" id="reply-email" placeholder="Email">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="reply-title">Título <sup>*</sup>
                                    </label>
                                    <input type="text" name="titulo" class="form-control" id="reply-title" placeholder="Título">
                                </div>
                                <!-- /.form-group -->

                                <div class="form-group">
                                    <label for="reply-text">Comentario <sup>*</sup>
                                    </label>
                                    <textarea name="comentario" class="form-control" id="reply-text" rows="7" placeholder="Tu comentario"></textarea>
                                </div>

                                <div class="pull-right">
                                    <input type='hidden' name='productos_id' value='<?= $producto->id ?>'>
                                    <button type="submit" class="submit btn btn-lg btn-default" id='sendcommentbutton'>ENVIAR COMENTARIO</button>
                                </div>
                                </div>
                                <!-- /.form-submit -->
                            </form>
                            <!-- /form -->
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>

        <div class="product-socials">
            <ul class="list-socials">
                          <li><a href="https://www.facebook.com/mallorcaislandfestival" title=""style=""><i class="icon icon-facebook"></i></a>
                            </li>
                              <li><a href="https://soundcloud.com/finalia" title=""style=""><i class="fa fa-soundcloud"></i></a>
                            </li>
                            <li><a href="https://www.instagram.com/mallorca_island_festival/" title=""style=""><i class="fa fa-instagram"></i></a>
                            </li>
                            <li><a href="https://www.youtube.com/user/FINALIAviajes" title=""style=""><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
        </div>
        <!-- /.product-socials -->

        <div class="relared-products">
            <div class="relared-products-header margin-bottom-50">
                <h3 class="upper">PRODUCTOS RELACIONADOS</h3>
            </div>

            <div class="container-fluid">
                <?php 
                    $this->db->where('categorias_id',$producto->categoria_id); 
                    $this->db->limit(4);
                    foreach($this->querys->getProductos()->result() as $p):                     
                ?>
                <?php $this->load->view('includes/fragmentos/producto',array('p'=>$p,'class'=>'col-xs-12 col-sm-3')) ?>
                <?php endforeach ?>
            </div>
        </div>
        <!-- /.relared-products -->
    </div>
    <?php $this->load->view('includes/template/footer',array()); ?>
    <script>
        $(function() { aweProductRender(); });
        function sendComment(){
            $("#sendcommentbutton").attr('disabled');
            form = new FormData(document.getElementById('form'));
            $.ajax({
                url:'<?= base_url('main/addComment') ?>',
                data:form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST'            
            }).always(function(data){
                if(data==='success'){
                    document.location.href="<?= site_url('productos/'. toURL($producto->producto_nombre).'-'.$producto->id).'/1' ?>/";
                }
            });
            return false;
        }
    </script>
