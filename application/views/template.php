<!DOCTYPE html>
<html lang="en-US">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= empty($title)?'Mallorca Island Festival':$title ?></title>               
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/plugins.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/styles.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/style.css">
        <script src="<?= base_url() ?>js/vendor.js"></script>
        <script>
            window.SHOW_LOADING = false;
        </script>
</head>

<body class="blog wpb-js-composer js-comp-ver-4.5.3 vc_responsive" >  
    <!-- // LOADING -->
    <div class="awe-page-loading">
        <div class="awe-loading-wrapper">
            <div class="awe-loading-icon">
                <?= img('img/logo.jpg','width:200px') ?>
            </div>

            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    </div>
    <!-- // END LOADING -->
    <div id="wrapper" class="main-wrapper ">
        <?php $this->load->view($view) ?>
    </div>
    <!-- /.login-popup -->
    <script>
        $(function() {
            $('a[href="#login-popup"]').magnificPopup({
                type:'inline',
                midClick: false,
                closeOnBgClick: false
            });
        });
    </script>
    <!--<script type='text/javascript' src='//www.beetailer.com/javascripts/beetailer.js'></script>-->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>   
    <script src="<?= base_url() ?>js/admin/frame.js"></script>
    <script src="<?= base_url() ?>js/plugins.js"></script>
    <script src="<?= base_url() ?>js/main.js"></script>
    <script src="<?= base_url() ?>js/docs.js"></script>
    <script>
        function addToCart(producto_id,cantidad){
            $.post('<?= base_url() ?>main/addToCart/'+producto_id+'/'+cantidad,{},function(data){
                $(".menubar-cart").html(data);
                emergente("<div class='alert alert-warning'>Producto Añadido al carrito. El carrito lo encontraras en la parte superior derecha</div>");
            });
        }
        function remToCart(producto_id){
            $.post('<?= base_url() ?>main/delToCart/'+producto_id,{},function(data){
                $(".menubar-cart").html(data);
            });
        }
    </script>
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?5eWjra4dPF45svks9vl2RVHUElbzU7j9";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>

