<div class="product product-grid <?= empty($class)?'col-xs-12 col-sm-4':$class ?>">
<div class="product-media">
    <div class="product-thumbnail">
        <a href="<?= site_url('productos/'. toURL($p->producto_nombre).'-'.$p->id) ?>" title="">
            <img src='<?= base_url('images/productos/'.$p->foto) ?>' class="current">
        </a>
    </div>
    <!-- /.product-thumbnail -->


    <div class="product-hover">
        <div class="product-actions">
            <a href="javascript:addToCart('<?= $p->id ?>',1)" class="awe-button product-add-cart" data-toggle="tooltip" title="Añadir al carrito">
                <i class="icon icon-shopping-bag"></i>
            </a>                                       

            <a href="<?= site_url('productos/'. toURL($p->producto_nombre).'-'.$p->id) ?>" class="awe-button" data-toggle="tooltip" title="Vista Previa">
                <i class="icon icon-eye"></i>
            </a>
        </div>
    </div>
    <!-- /.product-hover -->



</div>
<!-- /.product-media -->

<div class="product-body">
    <h2 class="product-name">
        <a href="<?= site_url('productos/'. toURL($p->producto_nombre).'-'.$p->id) ?>" title="<?= $p->producto_nombre ?>"><?= $p->producto_nombre ?></a>
    </h2>
    <div style="height: 40px; color: rgb(137, 137, 137); box-sizing: border-box; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
        <?= $p->texto_corto ?>
    </div>
    <div class="product-price">

        <span class="amount">
            <del><?= $p->precio_sin_descuento!=0?moneda($p->precio_sin_descuento):'' ?></del>
            <?= moneda($p->precio) ?>
        </span>

    </div>
    <!-- /.product-price -->
</div>
<!-- /.product-body -->
</div>
<!-- /.product -->