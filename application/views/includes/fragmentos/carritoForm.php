<?php $carrito = $this->querys->getCarrito(); $total = 0;?>
<div class="row">
<?php foreach($carrito as $c): ?>
<div class="col-xs-12 col-sm-6" style="margin-bottom: 20px; min-height: 220px;">
    <div class="col-xs-1">
        <a href="javascript:delToCart('<?= $c->id ?>')" title="" class="remove">
            <i class="icon icon-remove"></i>
        </a>
    </div>
    <div class="col-xs-12 col-sm-3">
        <a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title="">
            <?= img('images/productos/'.$c->foto,'width:100%;') ?>
        </a>
    </div>
    <div class="col-xs-12 col-sm-8">
        <div class="whishlist-name" style="margin-top:20px">
            <h3 style="margin:0px;"><a href="<?= site_url('productos/'. toURL($c->producto_nombre).'-'.$c->id) ?>" title=""><?= $c->producto_nombre ?></a></h3>
        </div>
        <div class="form-group">
            <label for="preu" class="col-sm-4 control-label">Precio:</label>
            <div class="col-sm-5">
                <span style="font-size:20px"><strong><?= moneda($c->precio) ?></strong></span>
            </div>
        </div> 
        <div class="form-group">
            <label for="cantidad" class="col-sm-4 control-label">Cantidad:</label>
            <div class="col-sm-5">
                <input type="number" name="cantidad[]" class="form-control" value="<?= $c->cantidad ?>">
                <input type="hidden" name="id[]" class="cantidad" value="<?= $c->id ?>">
            </div>
            
        </div> 
        <?php if(!empty($c->color)): ?>
            <div class="form-group">
                <label for="color" class="col-sm-4 control-label">Color*:</label>
                <div class="col-sm-5">
                    <?php 
                        $items = explode(',',$c->color); 
                        echo '<select name="color[]" id="color" class="form-control">';
                        echo '<option value="">Escoge color</option>';
                        foreach($items as $i){
                            if(!empty($c->colorselected) && $c->colorselected==$i){
                                echo '<option selected>'.$i.'</option>';
                            }else{
                                echo '<option>'.$i.'</option>';
                            }
                        }
                        echo '</select>';
                    ?>
                </div>
            </div>
        <?php else: ?>
        <input type="hidden" name="color[]" id="color" value=" ">
        <?php endif ?> 
        
        <?php if(!empty($c->talla)): ?>
            <div class="form-group">
                <label for="talla" class="col-sm-4 control-label">Talla*:</label>
                <div class="col-sm-5">
                    <?php 
                        $items = explode(',',$c->talla); 
                        echo '<select name="talla[]" id="talla" class="form-control">';
                        echo '<option value="">Escoge talla</option>';
                        foreach($items as $i){
                            if(!empty($c->tallaselected) && $c->tallaselected==$i){
                                echo '<option selected>'.$i.'</option>';
                            }else{
                                echo '<option>'.$i.'</option>';
                            }
                        }
                        echo '</select>';
                    ?>
                </div>
            </div>
        <?php else: ?>
        <input type="hidden" name="talla[]" id="talla" value=" ">
        <?php endif ?>
                
    </div>                                
</div>
<?php $total+= ($c->cantidad*$c->precio); ?>
<?php endforeach ?>
</div>
<?php if($total>0): ?>
<?php 
    $ajustes = $this->db->get('ajustes')->row();
    if(strtotime(date("Y-m-d")) >= strtotime($ajustes->suspender_cobro_envio_desde) && strtotime(date("Y-m-d")) <= strtotime($ajustes->suspender_cobro_envio_hasta)){
        $impuesto = 0;
    }else{
        $impuesto = 4.95;
    }    
    $impuesto = $total<25?$impuesto:0; 
?>
<div class="row menu-cart-total" style="text-align:right">
    <div class="col-xs-12 col-sm-9">
    <div class="col-xs-12 col-sm-6">
            <textarea name='observaciones' class="form-control" placeholder="Observaciones" style=" height: 140px; width:320px"></textarea>
        </div>
        <div class="col-xs-12 col-sm-6">
             
            
        </div>
       
    </div>
    <div class="col-xs-12 col-sm-3" align="right" style="margin-top:30px">
        <div>
            <span style="font-family: open sans; ">Subtotal</span>
            <span style="font-family: open sans;"><?= moneda($total) ?></span>
        </div>
        <div>
            <span style="font-family: open sans;">Portes</span>
            <span style="font-family: open sans;" id="costo_envio"><?= moneda($impuesto) ?></span>
        </div>
        <div>
            <span style="font-family: open sans;">Total</span>
            <span style="font-size:40px; font-family: montserratBold; color: white" id="total"><?= moneda($total+$impuesto) ?></span>
        </div>
        <div class="field field_aceptat">
                <input type="checkbox" id="politicas" name="politicas">
                <label for="politicas">Acepto las</label>&nbsp;<a href="javascript:politicas()"style="font-family: open sans">Condiciones de venta</a><span class="error"></span>                
            </div>
    </div>
</div>
<div align="right">
<!--<a href="<?= empty($_SESSION['user'])?site_url('registro/index/add?redirect=usuario/comprar'):base_url('usuario/comprar') ?>" title="" class="btn btn-lg btn-primary">Proceder a pagar</a>-->    
    <input type="hidden" name="costo_envio" id="costo_envio_form" value="0">
<button type="submit" class="btn btn-lg btn-primary">FINALIZAR COMPRA</button>
</div>
<?php else: ?>
<div class="menu-cart-total">
<span>Carrito vacio</span>                    
</div>
<?php endif ?>
<script>
    var total = <?= $total ?>;
    $(document).on('ready',function(){
       $("#provincia").on('change',function(){
           //alert(total+$("#provincia :selected").data('val'));
           var costo_envio = parseFloat($("#provincia :selected").data('val'));
           var t = total+parseFloat($("#provincia :selected").data('val'));
           $("#costo_envio").html(costo_envio.formatMoney(2,',','.')+' €');
           $("#total").html(t.formatMoney(2,',','.')+' €');
           $("#costo_envio_form").val(costo_envio);
       }) 
    });
</script>
