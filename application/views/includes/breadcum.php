<?php if($this->user->log): ?>
<div class="breadcrumbs" id="breadcrumbs">
        <ul class="breadcrumb">
                <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                </li>
                <li class="active"><?= empty($title)?'Consola':$title ?></li>
        </ul><!-- /.breadcrumb -->
</div>
<?php endif ?>