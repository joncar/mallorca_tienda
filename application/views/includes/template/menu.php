<div class="navbar  navbar-default ">

  <div class="mobile-menu">MENU</div>


    <div class="navbar yamm navbar-default ">
        <div class="container">
            <nav id="navbar-collapse-1" class="navbar-collapse collapse">
                <button type="button" class="hamburger is-closed" data-toggle="offcanvas"> <span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span> </button>
                <ul id="main-menu" class="nav navbar-nav">
                    <li class="menu-item">
                       <a href="<?= site_url() ?>">Inicio</a>
                    </li>
                    <li class="menu-item">
                       <a href="<?= site_url('pagina-nosotros') ?>">Quienes Somos</a>
                    </li>
                    <li class="menu-item">
                       <a href="<?= site_url('ver-eventos') ?>">Eventos</a>
                    </li>
                    <li class="menu-item">
                       <a href="<?= site_url('pagina-convocatorias') ?>">Convocatorias</a>
                    </li>
                    <li class="menu-item">
                       <a href="<?= site_url('fotos') ?>">Galería</a>
                    </li>
                    <li class="menu-item">
                       <a href="<?= site_url('noticias') ?>">Noticias</a>
                    </li>
                    <li class="menu-item">
                       <a href="<?= site_url('contacto') ?>">Contacto</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>