<footer class="footer">
<div class="footer-wrapper">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 col-sm-6">

                            <div class="widget">
                                <h3 class="widget-title">CÓMO COMPRAR</h3>

                                <div class="widget-content">
                                    <p style=" color: whitesmoke" > Escoge el artículo que deseas y añádelo a tu carrito. </br>Clicka en el icono de la cesta situado en la parte superior derecha de la pantalla para ir a tu carrito de la compra. </br>Finaliza la compra y regístrate para recibir tu pedido en 48/72 horas. Para pedidos inferiores a 25€ el envío tendrá un coste de 4,95€. Miféalo! </p>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 col-sm-6">

                            <div class="widget">
                            <i class="fa fa-info-circle" style=" font-size: 28px; margin-bottom: 10px"></i>
                                <h3 class="widget-title"  >ATENCIÓN TELEFÓNICA</h3>

                                <div class="widget-content">
                                    <p style=" color: whitesmoke">de Lunes a Viernes de 08:00-21:00</p>
                                    <p style="font-size: 24px; color: whitesmoke; font-weight: 600">902 002 068</p>
                                 
                                </div>
                            </div>
                            <!-- /.widget -->

                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">

                    <div class="widget">
                    <i class="icon icon-user-circle" style=" font-size: 28px; margin-bottom: 10px"></i>
                        <h3 class="widget-title">TU INFO</h3>

                        <ul>
                            <li><a href="<?= base_url('main/carrito') ?>" title="" style="  font-weight: 500">Tu Carrito</a>
                            </li>
                            <li><a href="<?= base_url('usuario/ventas') ?>" title="" style="font-weight: 500">Tus Pedidos</a>
                            </li>
                        	<li><a href="<?= base_url('main/unlog') ?>" title="" style="font-weight: 500">Cerrar Sesión</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.widget -->

                </div>

                <div class="col-md-2 col-sm-6">

                    <div class="widget">
                    <i class="fa fa-external-link-square" style=" font-size: 28px; margin-bottom: 10px"></i>
                        <h3 class="widget-title">MÁS LINKS</h3>

                        <ul>
                            <li><a href="http://www.mallorcaislandfestival.com" title="" style="  font-weight: 500">MIF web</a>
                            </li>
							<li><a href="http://mallorcaislandfestival.com/que-es-mif.html" title="" style=" font-weight: 500">¿Qué es MIF?</a>
                            </li>
                            <li><a href="http://mallorcaislandfestival.com/store/" title="" style=" font-weight: 500">Reserva Online</a>
                            </li>
                            <li><a href="http://mallorcaislandfestival.com/concurso-mif.html" title="" style="  font-weight: 500">Concurso MIF</a>
                            </li>
                            <li><a href="http://mallorcaislandfestival.com/trabaja-con-nosotros.html" title="" style="  font-weight: 500">Trabaja con nosotros</a>
                            </li>
                            
                        </ul>
                    </div>
                    <!-- /.widget -->

                </div>

                <div class="col-md-3">

                    <div class="widget">
                    <i class="fa fa-share-alt-square" style=" font-size: 28px; margin-bottom: 10px"></i>
                        <h3 class="widget-title">REDES SOCIALES</h3>

                        <ul class="list-socials">
                          <li><a href="https://www.facebook.com/mallorcaislandfestival" title=""style=""><i class="icon icon-facebook"></i></a>
                            </li>
                              <li><a href="https://soundcloud.com/finalia" title=""style=""><i class="fa fa-soundcloud"></i></a>
                            </li>
                            <li><a href="https://www.instagram.com/mallorca_island_festival/" title=""style=""><i class="fa fa-instagram"></i></a>
                            </li>
                            <li><a href="https://www.youtube.com/user/FINALIAviajes" title=""style=""><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>



                    <div class="widget">
                    <i class="fa fa-globe" style=" font-size: 28px; margin-bottom: 10px"></i>
                        <h3 class="widget-title">GRUPO FINALIA</h3>

                        <ul class="list-socials">
                            <li>
                                <a title="transferència"> 
                                    </i> <span style=" font-size: 14px; font-weight: 500; color: whitesmoke /* color: whitesmoke" */ ">   C/ GIRONA, 34 <br> 08700 Igualada. Spain</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                    <!-- /.widget -->

                </div>
            </div>
        </div>


    </div>
    <!-- /.footer-widgets -->

    <div class="footer-copyright">
        <div class="container">
            <div class="copyright">
                <p>Copyright &copy; 2016 Mallorca Island Festival</p>
            </div>

            <div class="footer-nav">
                <nav>
                    <ul>
                        <li><a href="mailto:tienda@mallorcaislandfestival.com" title="">Contacto</a>
                        </li>
                        <li><a href="javascript:legal()" title="">Aviso legal</a>
                        </li>
                    </ul>
                </nav>                
            </div>
        </div>
    </div>
    <!-- /.footer-copyright -->
</div>
<!-- /.footer-wrapper -->

<a href="#" class="back-top" title="">
    <span class="back-top-image">
        <img src="<?= base_url('img/back-top.png') ?>" alt="">
    </span>

    <small>Ir arriba</small>
</a>
<!-- /.back-top -->
</footer>
<script>
    function legal(){
        $.post('<?= base_url() ?>main/mostrarlic',{},function(data){
                 emergente(data);
        });
    }
</script>