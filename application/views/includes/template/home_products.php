<section>
    <div class="container">
        <div class="home-products padding-vertical-60">
            <?php foreach($this->querys->getCategorias()->result() as $c): ?>
                
                <?php 
                    $this->db->where('categorias_id',$c->id);
                    if(empty($_GET)){
                        $this->db->limit('9');
                    }
                    $producto = $this->querys->getProductos();                     
                    if($producto->num_rows>0):                     
                ?>
            <div class="row" style="border-bottom:1px solid #c9c9c9; margin-top:70px;">
                
                <div class="col-md-3 col-sm-4">
                    <div class="awe-media home-cate-media">
                        <div class="awe-media-header">
                            <div class="awe-media-image">
                                <img src='<?= base_url('images/categorias/'.$c->foto) ?>' class="current">                                
                            </div>
                            <!-- /.awe-media-image -->

                            <div class="awe-media-overlay overlay-dark-50 fullpage">
                                <div class="content">
                                    <div class="fp-table text-left">
                                        <div class="fp-table-cell">

                                            <h2 class="upper"><?= $c->categoria_nombre ?></h2>                                            
                                            <a href="<?= base_url('categorias/'.toURL($c->categoria_nombre).'-'.$c->id) ?>" class="btn btn-sm btn-outline btn-white">Ver todos</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.awe-media-overlay -->
                        </div>
                        <!-- /.awe-media-header -->
                    </div>
                    <!-- /.awe-media -->
                </div>                
                <div class="col-md-9 col-sm-8">
                    <div class="products">
                        <?php foreach($producto->result() as $p):  ?>
                            <?php $this->load->view('includes/fragmentos/producto',array('p'=>$p)); ?>
                        <?php endforeach ?>     
                    </div>
                </div>
            </div>
            <?php endif ?>
            <!-- /.row -->
            <?php endforeach ?>
        </div>
        <!-- /.home-products -->

    </div>
    <!-- /.container -->
</section>